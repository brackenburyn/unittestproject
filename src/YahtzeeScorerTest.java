import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by noah2 on 1/10/17.
 */
public class YahtzeeScorerTest {
    private YahtzeeScorer ys;

    @Before
    public void setUp() throws Exception {
        ys = new YahtzeeScorer();
    }

    @Test
    public void canGetScore(){
        ys.returnScore();
    }

    @Test
    public void canScoreYahtzee(){
        ys.scoreYahtzee(1, 2, 3, 4, 5);
    }

    @Test
    public void noYahtzeeReturnsZero(){
        int result = ys.scoreYahtzee(1, 2, 3, 4, 5);
        assertEquals(0, result);
    }

    @Test
    public void yahtzeeReturnsFifty(){
        int result = ys.scoreYahtzee(2, 2, 2, 2, 2);
        assertEquals(50, result);
    }

    @Test
    public void canScoreLgStrt(){
        ys.scoreLgStrt(1, 2, 3, 4, 5);
    }

    @Test
    public void noLgStrtReturnsZero(){
        int result = ys.scoreLgStrt(1, 3, 4, 5, 5);
        assertEquals(0, result);
    }

    @Test
    public void lgStrtReturnsForty(){
        int result = ys.scoreLgStrt(2, 3, 4, 5, 6);
        assertEquals(40, result);
    }

    @Test
    public void canScoreFullHouse(){
        ys.scoreFullHouse(2, 2, 3, 3, 3);
    }

    @Test
    public void noFullHouseReturnZero(){
        int result = ys.scoreFullHouse(2, 3, 4, 5, 6);
        assertEquals(0, result);
    }

    @Test
    public void fullHouseReturnsTwentyFive(){
        int result = ys.scoreFullHouse(1, 1, 6, 6, 6);
        assertEquals(25, result);
    }

    @Test
    public void canScoreThreeOfKind(){
        ys.scoreThreeOfKind(1, 1, 1, 1, 1);
    }

    @Test
    public void noThreeOfKindReturnsZero(){
        int result = ys.scoreThreeOfKind(1, 1, 2, 6, 6);
        assertEquals(0, result);
    }

    @Test
    public void threeOfKindReturnsScore(){
        int result = ys.scoreThreeOfKind(1, 1, 5, 5, 5);
        assertEquals(17, result);
    }

    @Test
    public void getInitialScoreReturnsZero() {
        int result = ys.returnScore();
        assertEquals(0, result);
    }

    @Test
    public void getFinalScoreReturnsScore(){
        ys.scoreYahtzee(2, 2, 2, 2, 2);
        ys.scoreLgStrt(2, 3, 4, 5, 6);
        ys.scoreFullHouse(1, 1, 6, 6, 6);
        ys.scoreThreeOfKind(1, 1, 5, 5, 5);
        int result = ys.returnScore();
        assertEquals(132, result);
    }

    @Test
    public void scoredYahtzeeReturnsHundred(){
        int result = ys.scoreYahtzee(2, 2, 2, 2, 2);
        result = ys.scoreYahtzee(2, 2, 2, 2, 2);
        assertEquals(100, result);
    }

    @Test
    public void scoredLgStrtReturnsError(){
        int result = ys.scoreLgStrt(2, 3, 4, 5, 6);
        result = ys.scoreLgStrt(1, 2, 3, 4, 5);
        assertEquals(-1, result);
    }

    @Test
    public void scoredFullHouseReturnsError(){
        int result = ys.scoreFullHouse(4, 2, 4, 2, 2);
        result = ys.scoreFullHouse(5, 2, 2, 5, 2);
        assertEquals(-1, result);
    }

    @Test
    public void scoredThreeOfKindReturnsError(){
        int result = ys.scoreThreeOfKind(2, 2, 1, 2, 3);
        result = ys.scoreThreeOfKind(2, 2, 1, 2, 3);
        assertEquals(-1, result);
    }

}
