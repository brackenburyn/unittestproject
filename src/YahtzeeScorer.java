import java.util.*;
/**
 * Created by noah2 on 1/10/17.
 */
public class YahtzeeScorer {

    public int yahtzeeScore;
    public int lgStrtScore;
    public int fullHouseScore;
    public int threeOfKindScore;

    public YahtzeeScorer() {
        fullHouseScore = -1;
        lgStrtScore = -1;
        yahtzeeScore = -1;
        threeOfKindScore = -1;
    }

    public int scoreYahtzee(int i, int i1, int i2, int i3, int i4) {
        if (this.yahtzeeScore >=0) {
            System.out.println("You scored another yahtzee!");
            this.yahtzeeScore += 100;
            return 100;
        }
        if (i == i1 && i == i2 && i == i3 && i == i4){
            this.yahtzeeScore=50;
            return 50;
        }
        this.yahtzeeScore=0;
        return 0;
    }

    public int scoreLgStrt(int i, int i1, int i2, int i3, int i4) {
        if (this.lgStrtScore >=0) {System.out.println("You have already scored this category");return -1;}
        int[] dice = {i, i1, i2, i3, i4};
        if ((Arrays.binarySearch(dice, 2) >= 0) && (Arrays.binarySearch(dice, 3) >= 0) &&
                (Arrays.binarySearch(dice, 4) >= 0) && (Arrays.binarySearch(dice, 5) >= 0) &&
                ((Arrays.binarySearch(dice, 1) >= 0) || (Arrays.binarySearch(dice, 6) >= 0))){
            this.lgStrtScore=40;
            return 40;
        }
        this.lgStrtScore=0;
        return 0;
    }


    public int scoreFullHouse(int i, int i1, int i2, int i3, int i4) {
        if (this.fullHouseScore >=0) {System.out.println("You have already scored this category");return -1;}
        int[] dice = {i, i1, i2, i3, i4};
        int[] tally = new int[6];
        int[] freqs = new int[6];
        for (int n = 0; n < dice.length; n++){tally[dice[n]-1] += 1;}
        for (int m = 0; m < tally.length; m++){freqs[tally[m]] += 1;}
        if ((freqs[2] > 0) && (freqs[3] >= 0)) {this.fullHouseScore=25;return 25;}
        this.fullHouseScore=0;
        return 0;
    }

    public int scoreThreeOfKind(int i, int i1, int i2, int i3, int i4) {
        if (this.threeOfKindScore >=0) {System.out.println("You have already scored this category");return -1;}
        int[] dice = {i, i1, i2, i3, i4};
        int[] tally = new int[6];
        int[] freqs = new int[6];
        int score = 0;
        for (int n = 0; n < dice.length; n++){
            tally[dice[n]-1] += 1;
            score += dice[n];
        }
        for (int m = 0; m < tally.length; m++){freqs[tally[m]] += 1;}
        if (freqs[3] > 0) {
            this.threeOfKindScore=score;
            return score;
        }
        this.threeOfKindScore=0;
        return 0;
    }

    public int returnScore() {
        int score = 0;
        if (this.yahtzeeScore >= 0) score += this.yahtzeeScore;
        if (this.lgStrtScore >= 0) score += this.lgStrtScore;
        if (this.fullHouseScore >= 0) score += this.fullHouseScore;
        if (this.threeOfKindScore >= 0) score += this.threeOfKindScore;
        return score;
    }
}
